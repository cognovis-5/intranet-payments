# /packages/intranet-payments/www/record-payment.tcl

ad_page_contract {
    Purpose: records payment

} {
  cost_id:integer
  note
  received_date
  amount
  payment_type_id
  {return_url ""}
}

# ---------------------------------------------------------------
# Defaults & Security
# ---------------------------------------------------------------

# User id already verified by filters
set user_id [ad_maybe_redirect_for_registration]
if {![im_permission $user_id add_payments]} {
    ad_return_complaint "Insufficient Privileges" "
    <li>You don't have sufficient privileges to see this page."    
}

if { ![ad_var_type_check_number_p $amount] } {
    ad_return_complaint 1 "
    <li>The value \"amount\" entered from previous page must be a valid number."
    return
}

if { $amount < 0 } {
    ad_return_complaint 1 "
    <li>The value \"amount\" entered from previous page must be non-negative."
    return
}

im_payment_create_payment -cost_id $cost_id -note $note -received_date $received_date -actual_amount $amount -payment_type_id $payment_type_id

if {"" == $return_url} {
	set return_url [export_vars -base "/intranet-invoices/view" -url {{invoice_id $cost_id}}]
}

ad_returnredirect $return_url
